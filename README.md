# Ides
#### An open-source discord bot to view official Pinewood Builders events.
## Invite
https://discord.com/api/oauth2/authorize?client_id=841148794498580480&permissions=2147863616&scope=bot
## Changelogs

## Usage

**Prefix**
> '**s!**'

### **schedule**
**Aliases**  
> ['**s**']

**Argument 1**
> Division (**Mandatory Argument**)

**Argument 2**
> Filter/Events shown (Optional Argument, if not supplied, all the events will be shown)

**Usage**
> s!s PBST 5  
> *Shows **5** events from the security schedule*

> s!s TMS host=EnthusiasticKuba4007  
> *Shows **ALL** events hosted by EnthusiasticKuba4007 in the Syndicates Schedule*

### nextEvent
**Aliases**
> ['**ne**']

**Argument 1**
> Division (**Mandatory Argument**)
 
**Usage**
> s!ne PBST  
> *Shows **the next** event from the security schedule*

